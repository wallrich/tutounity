using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Renderer))]
[AddComponentMenu("Good Country flag")]
public class FlagMaterialGood : MonoBehaviour
{
    private string urlBase = "https://flagcdn.com/256x192/";

    private Material m = null;
    // Start is called before the first frame update
    void Start()
    {
    }


    public void setFlag(string country)
    {
        Renderer r = GetComponent<Renderer>();
        if (r) m = r.material;

        if (!m) return;
        StartCoroutine(DownloadImage(urlBase + country + ".png"));
    }

    IEnumerator DownloadImage(string MediaUrl)
    {

        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.result == UnityWebRequest.Result.Success)
        {
            Texture myTexture = DownloadHandlerTexture.GetContent(request);

            m.SetTexture("_MainTex", myTexture);
            m.SetTextureScale("_MainTex", new Vector2(2f, 2f));
        }
    }
}
