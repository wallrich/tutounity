using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class Source
{
    public string country_code2;
    public string country_code3;
    public string country_name;
    public int country_count;    
}

[System.Serializable]
public class TargetApp
{
    public int port;
    public string protocol;
    public List<Source> sources = new List<Source>();

}

[System.Serializable]
public class LHSData
{
    public List<TargetApp> targetApps = new List<TargetApp>();

    public static LHSData CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<LHSData>(jsonString);
    }

}