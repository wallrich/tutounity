using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[AddComponentMenu("Master script")]

public class Master : MonoBehaviour
{

    public string Url = "https://visu.lhs.loria.fr/data/";

    public List<GameObject> spawnAreas = new List<GameObject>();

    private int spawnAreaIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(getInfoFromServer());
    }

    IEnumerator getInfoFromServer()
    {
        while (true)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(Url))
            {
                // Debug.Log("Send http request");

                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                string[] pages = Url.Split('/');

                switch (webRequest.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        Debug.LogError("Error: " + webRequest.error);
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        Debug.LogError("HTTP Error: " + webRequest.error);
                        break;
                    case UnityWebRequest.Result.Success:
                        LHSData d = LHSData.CreateFromJSON(webRequest.downloadHandler.text);
                        yield return dropBalls(d);
                        break;
                }
            }
            yield return new WaitForSeconds(1f);

        }
    }


    IEnumerator dropBalls(LHSData d)
    {
        for (int i = 0; i < d.targetApps.Count; i++)
        {
            TargetApp t = d.targetApps[i];
            for (int j = 0; j < t.sources.Count; j++)
            {
                Source s = t.sources[j];
                if ( s.country_count == 0 ) continue;

                spawnNewAttack(s.country_count, s.country_code2, s.country_name, t.port);
                yield return new WaitForSeconds(0.3f);
            }

        }

    }


    private void spawnNewAttack(int size, string country, string countryName, int port)
    {
        GameObject sa = spawnAreas[spawnAreaIndex++];
        sa.GetComponent<Spawn>().SpawnObject(size, country.ToLower(), countryName, port);

        // rotation beetween spawnArea 
        if (spawnAreaIndex >= spawnAreas.Count) spawnAreaIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
