using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Movement of the camera
/// </summary>
[AddComponentMenu("CameraGood movements")]
public class CameraMoveGood : MonoBehaviour
{

    private Vector3 point = Vector3.zero;
    private float speedMod = 1.0f;//a speed modifier
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            speedMod = 1f;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            speedMod = -1f;
        }
        transform.RotateAround(point, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
        transform.LookAt(point);
        
    }
}
