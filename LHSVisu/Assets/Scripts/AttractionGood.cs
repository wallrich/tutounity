using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("AttractionGood")]
[RequireComponent(typeof(Rigidbody))]

public class AttractionGood : MonoBehaviour
{
    [SerializeField, Tooltip("Attraction factor")]
    public float AttractionFactor = 1;
    [SerializeField, Tooltip("Minimum distance")]
    public float MinDistance = 1;
    [SerializeField, Tooltip("Maximum Force")]
    public float MaxForce = 1;
    public int port = 0;
    // Start is called before the first frame update
    public int count = 0;
    void Start()
    {
        StartCoroutine(attackRoutine());
    }

    public void setValues(int givenPort, int givenCount)
    {
        port = givenPort;
        count = givenCount;
    }


    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator attackRoutine()
    {
        while (true)
        {
            ComputeAttraction();
            yield return new WaitForSeconds(0.4f);
        }
    }

    private void ComputeAttraction()
    {

        if (count == 0) return;

        Vector3 force = Vector3.zero;

        GameObject[] spheres = GameObject.FindGameObjectsWithTag("balls");
        for (int i = 0; i < spheres.Length; i++)
        {
            GameObject sphere = spheres[i];

            // --- its me --
            if (sphere.name == name) continue;

            Attraction att = sphere.GetComponent<Attraction>();
            if (att.port == port)
            {

                float distance = Vector3.Distance(sphere.transform.position, transform.position);
                if (distance < (transform.localScale.x + sphere.transform.localScale.x + MinDistance)) continue;
                Vector3 f = Vector3.Normalize(sphere.transform.position - transform.position);
                float fo = (AttractionFactor * count * att.count) / Mathf.Pow(Vector3.Distance(sphere.transform.position, transform.position), 2f);
                if (fo > MaxForce) fo = MaxForce;
                if (fo < 0.1 ) continue;

                f = f * fo;
                force += f;
            }
            else
            {
                float distance = Vector3.Distance(sphere.transform.position, transform.position);
                if (distance > (transform.localScale.x + sphere.transform.localScale.x + MinDistance*3)) continue;
                Vector3 f = Vector3.Normalize(transform.position - sphere.transform.position);
                float fo = (AttractionFactor * count * att.count) / Mathf.Pow(Vector3.Distance(sphere.transform.position, transform.position), 2f);
                if (fo > MaxForce) fo = MaxForce;
                if (fo < 0.1 ) continue;
                f = f * fo;
                force += f;

            }

        }

        //        if ( port == 22 )
        //        Debug.Log("Add force "+force.x+", "+force.y+", "+force.z);

        GetComponent<Rigidbody>().AddForce(force);

    }


}
