using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TextMesh))]

[AddComponentMenu("Fixed text")]
public class FixedText : MonoBehaviour
{

    [SerializeField, Tooltip("Scale")]
    public float scale = 0.1f;
    [SerializeField, Tooltip("Toggle KeyCode")]
    public KeyCode ToggleKeyCode;

    private Vector3 relativePosition = Vector3.zero;

    private GameObject mainCamera;

    private TextMesh t;
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.Find("Main Camera");

        relativePosition = transform.localPosition;

        // --- Set the scale independant from parent scale
        Vector3 scale = new Vector3(
            transform.lossyScale.x / transform.parent.lossyScale.x,
            transform.lossyScale.y / transform.parent.lossyScale.y,
            transform.lossyScale.z / transform.parent.lossyScale.z
        );
        transform.localScale = scale;

    }


    // Update is called once per frame
    void Update()
    {

        if (transform.lossyScale.x < scale)
        {
            float s = transform.localScale.x + 0.01f;
            transform.localScale = new Vector3(s, s, s);
        }
        if (transform.lossyScale.x > scale)
        {
            float s = transform.localScale.x - 0.01f;
            transform.localScale = new Vector3(s, s, s);
        }


        transform.position =
        transform.parent.transform.position +
        relativePosition * transform.parent.localScale.x;

        transform.LookAt(transform.position - mainCamera.transform.position);
    }

    public void SetText(string text)
    {
        t = GetComponent<TextMesh>();
        t.text = text;
    }
}
