using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Renderer))]
[AddComponentMenu("Country flag")]
public class FlagMaterial : MonoBehaviour
{
    private string urlBase = "https://flagcdn.com/256x192/";

    // Start is called before the first frame update
    void Start()
    {
    }


    public void setFlag(string country)
    {
        Renderer r = GetComponent<Renderer>();
        StartCoroutine(DownloadImage(urlBase + country + ".png"));
    }

    IEnumerator DownloadImage(string MediaUrl)
    {

        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();

        // Please fill here :)

    }
}
